#!/bin/env php
<?php
function main(&$argc, &$argv)
{
    if ($argc <= 1) {
        echo "[Type 'exit' to exit.]\n\n";

        while (true) {
            calc();
        }
    } else {
        calc($argv['1']);
    }
}

function calc($stdin = 0)
{
    if (!$stdin) {
        echo '; ';

        $stdin = fopen('php://stdin', 'r');
        $stdin = fgets($stdin);
        $stdin = str_replace(',', '.', $stdin);
    }

    if ($stdin === "exit\n") {
        exit;
    } elseif (alphaCheck($stdin) !== 0) {
        exit("Utilisez -h pour afficher cette aide \n - Calcule simple: ./math.sh 5+2*4/3\n - Calcule complex ./math.sh \"(5+2*4)/3\"");
    }

    $stdin = trim($stdin);
    $stdin = eval('return '.$stdin.';');

    echo ': ',$stdin,"\n\n";
}

function alphaCheck(&$calc)
{
    $result = preg_match('/[a-z]/i', $calc, $null);

    return ($result);
}

main($argc, $argv);
